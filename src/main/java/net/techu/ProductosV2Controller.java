package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductosV2Controller {

    private ArrayList<Producto> listaProductos = null;

    public ProductosV2Controller(){
        listaProductos = new ArrayList<>();
        listaProductos.add(new Producto(1,"PR1", 27.35));
        listaProductos.add(new Producto(2,"PR2", 17.50));

    }
    /*Get lista de productos*/
    @GetMapping(value = "/v2/productos", produces = "application/json")
    public ResponseEntity<List<Producto>> obtenerListado(){
        System.out.println("Estoy en consulta");
        return new ResponseEntity<>(listaProductos, HttpStatus.OK);
    }

    /* Get producto especifico*/
    //@RequestMapping(value="/productos/{id}", method = RequestMethod.GET)
    @GetMapping("/v2/productos/{id}")
    public ResponseEntity<Producto> obtenerProductoPorId(@PathVariable int id){

        Producto resultado = null;
        ResponseEntity<Producto> respuesta = null;
        try {
            resultado = listaProductos.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
            //respuesta = new ResponseEntity.ok(resultado);
        }
        catch (Exception e){
            //String mensaje = "No se ha encontrado producto";
            respuesta = new ResponseEntity(resultado, HttpStatus.NOT_FOUND);
            //respuesta = new ResponseEntity.notFound();
        }
        return  respuesta;
    }

    /*Add nuevo producto*/
    @PostMapping(value= "/v2/productos", consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> addProductos(@RequestBody Producto productoNuevo){
        System.out.println("Estot en add");
        System.out.println(productoNuevo.getId());
        System.out.println(productoNuevo.getNombre());
        System.out.println(productoNuevo.getPrecio());
        //listaProductos.add(new Producto(3,nombre, 27.35));;
        listaProductos.add(productoNuevo);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    /*Add nuevo producto con nombre*/
    //@RequestMapping(value = "/productos/{nom}/{cat}", method = RequestMethod.POST, produces = "application/json")
    @PostMapping(value= "/v2/productos/{nom}/{cat}",  produces = "application/json")
    public ResponseEntity<String> addProductosConNombre(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria){
        System.out.println("Voy agregar el producto : " + nombre + "  Y categoria:  " + categoria);
        listaProductos.add(new Producto(4,"PR4", 27.35));
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    /*Modificación de productos*/
    @PutMapping(value = "/v2/productos/{id}",produces = "application/json")
    public ResponseEntity<String> updateProductos(@PathVariable int id, @RequestBody Producto productoModificado){

        ResponseEntity<String> respuesta = null;
        try {
            Producto prodAMod = listaProductos.get(id);
            System.out.println("Se va a modificar un producto");
            System.out.println("Precio actual: " + String.valueOf(prodAMod.getPrecio()));
            System.out.println("Precio nuevo:  " + String.valueOf(productoModificado.getPrecio()));
            prodAMod.setNombre(productoModificado.getNombre());
            prodAMod.setPrecio(productoModificado.getPrecio());
            listaProductos.set(id, prodAMod);
            respuesta = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception e){
            respuesta = new ResponseEntity<>("No se ha encontrado producto", HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    /*Eliminación de producto*/
    @DeleteMapping(value = "/v2/productos/{id}",produces = "application/json")
    public  ResponseEntity<String> eliminarProductos(@PathVariable int id){

        ResponseEntity<String> respuesta = null;
        try {
            Producto prodAEliminar = listaProductos.get(id - 1);
            System.out.println("Se va a eliminar el producto: " + prodAEliminar.getNombre());
            listaProductos.remove(id - 1);
            respuesta = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception e){
            respuesta = new ResponseEntity<>("No se ha encontrado producto", HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    /*Eliminación de todos los producto*/
    @DeleteMapping(value = "/v2/productos",produces = "application/json")
    public  ResponseEntity<String> eliminarProductos(){

        ResponseEntity<String> respuesta = null;

        System.out.println("Se va a eliminar todos los productos: ");
        listaProductos.clear();
        respuesta = new ResponseEntity<>("Se ha Eliminado todos los productos: ", HttpStatus.OK);

        return respuesta;
    }
}
