package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductosController {

    private ArrayList<String> listaProductos = null;

    public  ProductosController(){
        listaProductos = new ArrayList<>();
        listaProductos.add("PR1");
        listaProductos.add("PR2");

    }
    /*Get lista de productos*/
    @GetMapping(value = "/productos", produces = "application/json")
    public ResponseEntity<List<String>> obtenerListado(){
        System.out.println("Estoy en consulta");
        return new ResponseEntity<List<String>>(listaProductos, HttpStatus.OK);
    }

    /* Get producto especifico*/
    //@RequestMapping(value="/productos/{id}", method = RequestMethod.GET)
    @GetMapping("/productos/{id}")
    public ResponseEntity<String> obtenerProductoPorId(@PathVariable int id){

        String resultado = null;
        ResponseEntity respuesta = null;
        try {
            resultado = listaProductos.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
            //respuesta = new ResponseEntity.ok(resultado);
        }
        catch (Exception e){
            resultado = "No se ha encontrado producto";
            respuesta = new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
            //respuesta = new ResponseEntity.notFound();
        }
        return  respuesta;
    }

    /*Add nuevo producto*/
    @PostMapping(value= "/productos", produces = "application/json")
    public ResponseEntity<String> addProductos(){
        System.out.println("Estot en add");
        listaProductos.add("nuevo");
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }
    /*Add nuevo producto con nombre*/
    //@RequestMapping(value = "/productos/{nom}/{cat}", method = RequestMethod.POST, produces = "application/json")
    @PostMapping(value= "/productos/{nom}/{cat}",  produces = "application/json")
    public ResponseEntity<String> addProductosConNombre(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria){
        System.out.println("Voy agregar el producto : " + nombre + "  Y categoria:  " + categoria);
        listaProductos.add(nombre);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    /*Modificación de productos*/
    @PutMapping(value = "/productos/{id}/{val}",produces = "application/json")
    public ResponseEntity<String> updateProductos(@PathVariable int id, @PathVariable String val){

        ResponseEntity<String> respuesta = null;
        try {
            String prodAMod = listaProductos.get(id);
            respuesta = new ResponseEntity<>("Producto actualizado correctamente", HttpStatus.OK);
        }
        catch (Exception e){
            respuesta = new ResponseEntity<>("No se ha encontrado producto", HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    /*Eliminación de productos*/
    @DeleteMapping(value = "/productos/{id}",produces = "application/json")
    public  ResponseEntity<String> eliminarProductos(@PathVariable int id){

        ResponseEntity<String> respuesta = null;
        try {
            String prodAEliminar = listaProductos.get(id);
            respuesta = new ResponseEntity<>("Se ha Eliminado el producto: ", HttpStatus.OK);
        }
        catch (Exception e){
            respuesta = new ResponseEntity<>("No se ha encontrado producto", HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }
}
